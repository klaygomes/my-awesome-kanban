export const Cards: Card[] = [
  {
    id: '9c9b1270-cf7f-4e96-ab33-9967a6b1d637',
    title: 'Todo Item 1',
    body: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent non ante et leo suscipit malesuada.',
    kind: 'ToDo',
    order: 0
  },
  {
    id: '9c9b1270-cf7f-4e96-ab33-9967a6b1d638',
    title: 'Todo Item 2',
    body: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent non ante et leo suscipit malesuada.',
    kind: 'ToDo',
    order: 1
  },
  {
    id: '9c9b1270-cf7f-4e96-ab33-9967a6b1d639',
    title: 'Todo Item 3',
    body: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent non ante et leo suscipit malesuada.',
    kind: 'ToDo',
    order: 2
  },
  {
    id: '9c9b1270-cf7f-4e96-ab33-9967a6b1d647',
    title: 'Doing Item 1',
    body: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent non ante et leo suscipit malesuada.',
    kind: 'Doing',
    order: 3
  },
  {
    id: '9c9b1270-cf7f-4e96-ab33-9967a6b1d648',
    title: 'Doing Item 2',
    body: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent non ante et leo suscipit malesuada.',
    kind: 'Doing',
    order: 4
  },
  {
    id: '9c9b1270-cf7f-4e96-ab33-9967a6b1d649',
    title: 'Doing Item 3',
    body: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent non ante et leo suscipit malesuada.',
    kind: 'Doing',
    order: 5
  },
  {
    id: '9c9b1270-cf7f-4e96-ab33-9967a6b1d657',
    title: 'Done Item 1',
    body: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent non ante et leo suscipit malesuada.',
    kind: 'Done',
    order: 6
  },
  {
    id: '9c9b1270-cf7f-4e96-ab33-9967a6b1d658',
    title: 'Done Item 2',
    body: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent non ante et leo suscipit malesuada.',
    kind: 'Done',
    order: 7
  },
  {
    id: '9c9b1270-cf7f-4e96-ab33-9967a6b1d659',
    title: 'Done Item 3',
    body: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent non ante et leo suscipit malesuada.',
    kind: 'Done',
    order: 8
  }
]
