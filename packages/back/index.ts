import { config } from 'dotenv'
config()
import express from 'express'
import jwt from 'jsonwebtoken'
import { v4 as uuid } from 'uuid'
import cors from 'cors'
import { order } from '@ada-tech/core'

import { Cards } from './cards'

const app = express()
let cards = [...Cards]

app.use(cors())

app.use(express.json())

app.use((req, _, next) => {
  console.log(req.url, req.params, req.body)
  next()
})

app.post('/login', (req, res) => {
  const { user, password } = req.body
  const { DEFAULT_LOGIN, DEFAULT_PASSWORD, JWT_SECRET } = process.env
  if (user === DEFAULT_LOGIN && password === DEFAULT_PASSWORD) {
    const token = jwt.sign({ user: `${DEFAULT_LOGIN}` }, JWT_SECRET, { expiresIn: '1h' })
    return res.json({ user, password, token })
  }
  res.statusCode = 401
  res.end()
})

const jwtValidation = (req: express.Request, res: express.Response, next: express.NextFunction) => {
  try {
    const { JWT_SECRET } = process.env
    const auth = req.headers.authorization
    const token = auth?.replace('Bearer ', '')
    if (typeof token === 'string') {
      const decoded = jwt.verify(token, JWT_SECRET)
      if (typeof decoded !== 'string') {
        res.locals = { user: decoded.user }
        console.info('JWT Middleware - validated token for user: ' + decoded.user)
      } else {
        throw new Error('token with wrong format')
      }
    } else throw new Error('token not found')
  } catch (err) {
    console.info('JWT Middleware - error validating token\n' + err)
    res.sendStatus(401)
    return res.end()
  }
  next()
}

app.use(jwtValidation)

app.get('/cards', (req, res) => {
  res.json(cards)
  res.end()
})

app.post('/cards', (req, res) => {
  const { title, body, kind, id, order: cardOrder } = req.body
  if (title && body && kind && !id) {
    const card = { title, body, kind, id: uuid(), order: cardOrder }
    cards.splice(cardOrder, 0, card)
    res.status(201).json(order(cards))
  } else return res.sendStatus(400)
})

const validateAndLogAlterationOrDeletion = (
  req: express.Request,
  res: express.Response,
  next: express.NextFunction
) => {
  const urlID = req.params.id
  const dateTime = new Date().toLocaleString('pt-br')

  if (!urlID) return res.sendStatus(400)

  const card = cards.find(x => x.id === urlID)
  if (!card) return res.sendStatus(404)

  if (req.method === 'PUT') {
    const { title, body, kind, id } = req.body
    if (urlID !== id) return res.status(400).json({ error: 'ids não correspondem' })
    if (!(title && body && kind && id)) return res.sendStatus(400)
    console.info(`${dateTime} - Card ${urlID} - ${card.title} - Alterar`)
  } else if (req.method === 'DELETE') {
    console.info(`${dateTime} - Card ${urlID} - ${card.title} - Remover`)
  }

  next()
}

app.use('/cards/:id', validateAndLogAlterationOrDeletion)

app.put('/cards/:id', (req, res) => {
  const { title, body, kind, id, order } = req.body
  const card = cards.find(x => x.id === id)
  if (card) {
    card.title = title
    card.body = body
    card.kind = kind
    card.order = order
    return res.status(200).json(card)
  } else {
    res.sendStatus(404)
  }
})

app.delete('/cards/:id', (req, res) => {
  const { id } = req.params
  cards = order(cards.filter(x => x.id !== id))
  res.json(cards)
})

app.listen(5001, () => console.log('listening on http://localhost:5000'))
