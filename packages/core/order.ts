export const order = (cards: Card[]) => cards.map((card, order) => ({ ...card, order }))
