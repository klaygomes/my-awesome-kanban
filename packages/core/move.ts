import type { DraggableLocation } from '@hello-pangea/dnd'
import { reorder } from './reorder'
import { Config } from './config'

const isColumnKind = (kind: string): kind is ColumnKind => Config.columns.some(col => col.kind === kind)

/**
 * Move a card from one list to another.
 * @param {Card[]} list - The source list of cards.
 * @param {DraggableLocation} source - The location of the source card.
 * @param {DraggableLocation} destination - The location of the destination card.
 * @returns {Card[]} - The list with card moved sorted and updated.
 */
export const move = (list: Card[], source: DraggableLocation, destination: DraggableLocation): [Card[], Card] => {
  if (!isColumnKind(destination.droppableId)) return [list, list[source.index]]

  let idx = destination.index

  if (source.index < destination.index) {
    idx = idx - 1
  }

  const [result, card] = reorder(list, source.index, idx)

  card.kind = destination.droppableId

  return [result, card]
}
