export const Config = {
  columns: [
    {
      title: 'Todo',
      kind: 'ToDo'
    },
    {
      title: 'Doing',
      kind: 'Doing'
    },
    {
      title: 'Done',
      kind: 'Done'
    }
  ]
}
