/**
 * Reorder a given list of cards by removing the card at a specified startIndex and inserting it at a specified endIndex.
 * @param {Card[]} list - The list of cards to be reordered.
 * @param {number} startIndex - The starting position of the card to be moved.
 * @param {number} endIndex - The final position to insert the moved card.
 * @returns {[Card[], Card]} - The reordered list of cards.
 */
export const reorder = (list: Card[], startIndex: number, endIndex: number): [Card[], Card] => {
  const result = [...list]
  const [moved] = result.splice(startIndex, 1)
  result.splice(endIndex, 0, moved)

  const withOrder = result.map((card, order) => ({ ...card, order }))
  return [withOrder, withOrder[endIndex]]
}
