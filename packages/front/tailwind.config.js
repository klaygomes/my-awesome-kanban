/** @type {import('tailwindcss').Config} */
module.exports = {
  content: ['./src/**/*.{ts,tsx}'],
  safelist: [
    {
      pattern: /grid-cols-\d/
    }
  ],
  theme: {
    extend: {}
  },
  plugins: []
}
