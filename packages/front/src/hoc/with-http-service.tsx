import { DropResult } from '@hello-pangea/dnd'
import { move, order, reorder } from '@ada-tech/core'
import React from 'react'
import { HttpService, authHttpService } from '../service/http-service'
import { EmptyService } from '../service/empty-service'

import { showError } from '../utils/show-error'

import { Provider } from '../context'

// TODO: Implement refresh token when expired
const currentUser: User = {
  token: localStorage.getItem('token') || undefined
}

// although i'm hot wiring httpService. If needed, any 
// object that implements Crud<Card> can be used here.
// e.g: I could write an graphql impl 
const defaultService = currentUser.token ? new HttpService(currentUser) : EmptyService

// It is possible to move the core logic to a different HOC 
// and use ID to bind them together.
// I could write something like withService(SomeService)(MyComponent)
// But lets keep it for the sake of simplicity
export function withHttpService<T extends Record<string, unknown>>(WrappedComponent: React.ComponentType<T>) {
  return (props: T) => {
    const [cards, updateCards] = React.useState<Card[]>([])
    const [user, updateUser] = React.useState<User>(currentUser)
    const [service, setService] = React.useState<Crud<Card>>(defaultService)

    React.useEffect(() => {
      if (user.token) service.read().then(updateCards)
    }, [service, user])

    const onMove = React.useCallback(
      (result: DropResult) => {
        const { source, destination } = result
        let sortedList: Card[]
        let updatedCard: Card

        if (!destination) {
          return
        }

        if (source.droppableId === destination.droppableId) {
          ;[sortedList, updatedCard] = reorder(cards, source.index, destination.index)
        } else {
          ;[sortedList, updatedCard] = move(cards, source, destination)
        }
        // we use optimistic approach
        // we update first, if it returns an error we rollback
        updateCards(sortedList)
        service.update(updatedCard).catch(() => {
          showError('Connection error while moving.')
          updateCards([...cards])
        })
      },
      [cards, service]
    )

    const onEdit = React.useCallback(
      (card: Partial<Card>) => {
        const localCard = cards.find(c => c.id === card.id)
        const stale = localCard
        if (localCard) {
          Object.assign(localCard, card)
          service
            .update(localCard)
            .catch(() => {
              showError('Connection error while editing.')
              Object.assign(localCard, stale)
            })
            .finally(() => updateCards(cards))
        }
      },
      [cards, service]
    )

    const onAdd = React.useCallback(
      (card: Partial<Card>) => {
        const stale = [...cards]
        const optimisticCard = { ...card } as Card
        // fake id
        optimisticCard.id = new Date().getTime().toString(32)
        const optimisticCards = [...cards]
        const idx = cards.findIndex(c => c.kind === card.kind)
        optimisticCard.order = idx
        optimisticCards.splice(idx, 0, optimisticCard)
        updateCards(order(optimisticCards))
        service
          .create(optimisticCard)
          .then(newCard => {
            updateCards(order(newCard))
          })
          .catch(() => {
            showError('Connection error while adding.')
            updateCards(stale)
          })
      },
      [cards, service]
    )

    const onDiscard = React.useCallback(
      (card: Partial<Card>) => {
        const stale = [...cards]
        const optimistic = [...stale].filter(cd => cd.id !== card.id)
        updateCards(optimistic)
        service.discard(card).catch(() => {
          showError('Connection error while deleting.')
          updateCards(stale)
        })
      },
      [cards, service]
    )

    const onLogin = (user: User) => {
      authHttpService
        .login(user)
        .then(user => {
          updateUser(user)
          localStorage.setItem('token', String(user.token))
          setService(new HttpService(user))
        })
        .catch(() => {
          showError('Wrong user or password')
        })
    }

    return (
      <Provider
        value={{
          onMove,
          onAdd,
          onDiscard,
          onEdit,
          onLogin,
          cards,
          user
        }}
      >
        <WrappedComponent {...props} />
      </Provider>
    )
  }
}
