import React from 'react'
import ReactDOM from 'react-dom/client'
import './index.css'

import { Kanban } from './Kanban'
import { withHttpService } from './hoc/with-http-service'
import { CustomError } from './components/custom-error'

const root = ReactDOM.createRoot(document.getElementById('root') as HTMLElement)

const KanbanHttp = withHttpService(Kanban)

root.render(
  <React.StrictMode>
    <>
      <KanbanHttp />
      <CustomError />
    </>
  </React.StrictMode>
)
