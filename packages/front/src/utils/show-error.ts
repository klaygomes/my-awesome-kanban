export const showError = (message: string) => {
  const customEvent = new CustomEvent('customError', { detail: message })
  document.dispatchEvent(customEvent)
}
