import { showError } from './show-error'

describe('showError', () => {
  it('dispatches a customError event with the given message', () => {
    const message = 'error message'
    const spy = jest.spyOn(document, 'dispatchEvent')
    showError(message)

    expect(spy).toHaveBeenCalled()
    const event = spy.mock.calls[0][0]
    expect(event.type).toBe('customError')
    expect(event.detail).toBe(message)
  })
})
