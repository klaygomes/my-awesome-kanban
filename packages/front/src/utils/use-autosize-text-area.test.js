import { renderHook } from '@testing-library/react-hooks'
import { useAutosizeTextArea } from './use-autosize-text-area'

describe('useAutosizeTextArea', () => {
  it('updates the height of the textarea based on its content', () => {
    const textAreaRef = { style: {}, scrollHeight: 100 }
    const value = 'sample text'

    renderHook(() => useAutosizeTextArea(textAreaRef, value))

    expect(textAreaRef.style.height).toBe('102px')
  })

  it('does not update the height of the textarea if textAreaRef is null', () => {
    const textAreaRef = null
    const value = 'sample text'

    renderHook(() => useAutosizeTextArea(textAreaRef, value))

    expect(textAreaRef).toBeNull()
  })
})
