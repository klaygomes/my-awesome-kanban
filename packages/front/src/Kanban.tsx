import React from 'react'

import { Board } from './components/board'
import adaLogo from './assets/ada-logo.svg'
import { Config } from '@ada-tech/core/config'
import { Context } from './context'
import { ModalCard } from './components/modal-card'
import { ModalLogin } from './components/modal-login'
import { showError } from './utils/show-error'

function isColumns(input: Record<PropertyKey, unknown>): input is Parameters<typeof Board>[0]['columns'] {
  const validKeys = Object.keys(input)
  return Config.columns.some(col => validKeys.some(key => col.kind === key))
}

const EmptyCard: Card = {
  order: 0,
  title: '',
  body: '',
  kind: 'ToDo'
}

export const Kanban: React.FC = () => {
  const { onAdd, user, onLogin } = React.useContext(Context)

  const [cardKind, setCardKind] = React.useState<ColumnKind | undefined>()
  const mappedColumns = Config.columns.reduce((acc, cur) => ({ ...acc, [cur.kind]: cur }), {})

  // only show if we have at least one valid column
  if (!isColumns(mappedColumns)) return null

  function validCard(card: Card) {
    if (!card.title) {
      showError('Card title required')
      return false
    }
    if (!card.body) {
      showError('Card title body')
      return false
    }
    return true
  }

  function handleOnCancel() {
    setCardKind(undefined)
  }

  function handleRequestAddCard(kind: ColumnKind) {
    setCardKind(kind)
  }

  function handleOnConfirm(card: Card) {
    if (!validCard(card)) return
    onAdd(card)
    setCardKind(undefined)
  }

  function handleOnLogin(user: User) {
    onLogin(user)
  }

  if (!user.token) {
    return <ModalLogin onConfirm={handleOnLogin} user={user}></ModalLogin>
  }

  return (
    <>
      {cardKind && (
        <ModalCard card={{ ...EmptyCard, kind: cardKind }} onCancel={handleOnCancel} onConfirm={handleOnConfirm} />
      )}
      <div className="container mx-auto p-4">
        <img className="text-right" alt="ADA`s Logo" src={adaLogo} />
        <main className="py-6 text-white">
          <h1 className="text-4xl font-bold tracking-tight">Board</h1>
          <p className="text-lg font-medium text-gray-400">
            Welcome to our Kanban app! Get organized and increase your productivity by managing your tasks with ease.
          </p>
        </main>
        <div className={`grid gap-4 grid-cols-${Config.columns.length}`}>
          <Board columns={mappedColumns} requestAddCard={handleRequestAddCard} />
        </div>
      </div>
    </>
  )
}
