import fetch from 'cross-fetch'

export const API = process.env.REACT_APP_SERVICE_URL
export const CARD_API = `${API}/cards`

export class HttpService implements Crud<Card> {
  private buildRequestOptions(method: 'PUT' | 'POST' | 'DELETE' | 'GET', card?: Partial<Card>) {
    return {
      method,
      headers: {
        'Content-Type': 'application/json',
        'Authorization': `Bearer ${this.user.token}`
      },

      ...(card
        ? {
            body: JSON.stringify(card)
          }
        : {})
    }
  }
  constructor(private user: User) {}
  create = (card: Partial<Card>): Promise<Card[]> => {
    // ignore id, if informed
    delete card.id
    const requestOptions = this.buildRequestOptions('POST', card)
    return fetch(CARD_API, requestOptions).then(res => res.json())
  }
  read = () => {
    const requestOptions = this.buildRequestOptions('GET')
    return fetch(CARD_API, requestOptions).then(res => res.json())
  }
  update = (card: Partial<Card>) => {
    if (!card.id) return Promise.reject('Card id is required')
    const requestOptions = this.buildRequestOptions('PUT', card)
    return fetch(`${CARD_API}/${card.id}`, requestOptions).then(res => res.json())
  }
  discard = (card: Partial<Card>) => {
    if (!card.id) return Promise.reject('Card id is required')
    const requestOptions = this.buildRequestOptions('DELETE', card)
    return fetch(`${CARD_API}/${card.id}`, requestOptions).then(res => res.json())
  }
}

// TODO: Implement refresh token when expired
export const authHttpService: Authentication<User> = {
  login: function (user: User) {
    if (!user.user || !user.password) return Promise.reject(new Error('Credential are required'))
    const requestOptions = {
      method: 'POST',
      headers: { 'Content-Type': 'application/json' },
      body: JSON.stringify(user)
    }
    return fetch(`${API}/login`, requestOptions).then<User>(res => res.json())
  },
  logOut() {
    /**/
  }
}
