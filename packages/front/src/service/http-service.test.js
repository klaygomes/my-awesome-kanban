import { CARD_API, HttpService } from './http-service'
import crossFetch from 'cross-fetch'

jest.mock('cross-fetch', () => {
  //Mock the default export
  return {
    __esModule: true,
    default: jest.fn()
  }
})

describe('HttpService', () => {
  let user
  let httpService
  beforeEach(() => {
    user = { token: 'token' }
    httpService = new HttpService(user)
    jest.spyOn(global, 'fetch').mockImplementationOnce(() =>
      Promise.resolve({
        json: () => Promise.resolve([])
      })
    )
  })

  it('builds request options with correct method, headers, and body', () => {
    const card = { name: 'card name', description: 'card description' }
    const requestOptions = httpService.buildRequestOptions('POST', card)

    expect(requestOptions).toEqual({
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
        'Authorization': `Bearer ${user.token}`
      },
      body: JSON.stringify(card)
    })
  })

  it('builds request options without a body for GET requests', () => {
    const requestOptions = httpService.buildRequestOptions('GET')

    expect(requestOptions).toEqual({
      method: 'GET',
      headers: {
        'Content-Type': 'application/json',
        'Authorization': `Bearer ${user.token}`
      }
    })
  })

  it('creates a card', async () => {
    const card = { name: 'card name', description: 'card description' }
    const expectedCard = { ...card, id: 1 }
    const expectedCards = [expectedCard]

    crossFetch.mockResolvedValue({
      status: 200,
      json: () => expectedCards
    })

    const cards = await httpService.create(card)
    expect(cards).toEqual(expectedCards)

    expect(crossFetch).toHaveBeenCalledWith(CARD_API, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
        'Authorization': `Bearer ${user.token}`
      },
      body: JSON.stringify(card)
    })
  })

  it('reads cards', async () => {
    const card = { name: 'card name', description: 'card description' }
    const expectedCard = [{ ...card, id: 1 }]
    const expectedCards = [expectedCard]

    crossFetch.mockResolvedValue({
      status: 200,
      json: () => expectedCards
    })

    const cards = await httpService.read()
    expect(cards).toEqual(expectedCards)

    expect(crossFetch).toHaveBeenCalledWith(CARD_API, {
      method: 'GET',
      headers: {
        'Content-Type': 'application/json',
        'Authorization': `Bearer ${user.token}`
      }
    })
  })

  it('updates card', async () => {
    const card = { name: 'card name', description: 'card description', id: 1 }
    const expectedCard = { ...card }
    const expectedCards = [expectedCard]

    crossFetch.mockResolvedValue({
      status: 200,
      json: () => expectedCards
    })

    const cards = await httpService.update(card)
    expect(cards).toEqual(expectedCards)

    expect(crossFetch).toHaveBeenCalledWith(`${CARD_API}/${card.id}`, {
      method: 'PUT',
      headers: {
        'Content-Type': 'application/json',
        'Authorization': `Bearer ${user.token}`
      },
      body: JSON.stringify(expectedCard)
    })
  })

  it('discards a card', async () => {
    const card = { name: 'card name', description: 'card description', id: 1 }
    const expectedCard = { ...card }
    const expectedCards = [expectedCard]

    const noIDCard = {
      ...card,
      id: undefined
    }

    await expect(httpService.discard(noIDCard)).rejects.toMatch('Card id is required')

    crossFetch.mockResolvedValue({
      status: 200,
      json: () => expectedCards
    })

    const cards = await httpService.discard(card)
    expect(cards).toEqual(expectedCards)

    expect(crossFetch).toHaveBeenCalledWith(`${CARD_API}/${card.id}`, {
      method: 'DELETE',
      headers: {
        'Content-Type': 'application/json',
        'Authorization': `Bearer ${user.token}`
      },
      body: JSON.stringify(expectedCard)
    })
  })
})
