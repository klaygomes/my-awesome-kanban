export const EmptyService: Crud<Card> = {
  create: function (record: Partial<Card>): Promise<Card[]> {
    throw new Error('Function not implemented.')
  },
  read: function (): Promise<Card[]> {
    throw new Error('Function not implemented.')
  },
  update: function (record: Partial<Card>): Promise<Card> {
    throw new Error('Function not implemented.')
  },
  discard: function (record: Partial<Card>): Promise<Card[]> {
    throw new Error('Function not implemented.')
  }
}
