import { DropResult } from '@hello-pangea/dnd'
import React from 'react'

interface KanbanContext {
  onAdd: (card: Partial<Card>) => void
  onEdit: (card: Partial<Card>) => void
  onMove: (result: DropResult) => void
  onDiscard: (card: Partial<Card>) => void
  onLogin: (user: User) => void
  cards: Card[]
  user: User
}

class EmptyContext implements KanbanContext {
  cards = []
  user = {}
  onEdit(card: Partial<Card>) {
    throw new Error('Function not implemented.')
  }
  onAdd(card: Partial<Card>) {
    throw new Error('Function not implemented.')
  }
  onMove(result: DropResult) {
    throw new Error('Function not implemented.')
  }
  onDiscard(card: Partial<Card>) {
    throw new Error('Function not implemented.')
  }
  onLogin(user: User) {
    throw new Error('Function not implemented.')
  }
}

export const emptyContext = new EmptyContext()
export const Context = React.createContext<KanbanContext>(emptyContext)
export const { Provider, Consumer } = Context
