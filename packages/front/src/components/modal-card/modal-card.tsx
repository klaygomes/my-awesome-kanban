import React from 'react'

import { Modal } from '../modal'
import { ModalCardHeader as Header } from './modal-card-header'
import { ModalCardBody as Body } from './modal-card-body'
import { ModalCardFooter as Footer } from './modal-card-footer'

type ModalCardProps = { card: Card; onCancel: () => void; onConfirm: (card: Card) => void }
export const ModalCard = ({ card, onConfirm, onCancel }: ModalCardProps) => {
  const bodyRef = React.useRef<HTMLInputElement | null>(null)
  const [cardAffected, handleOnChange] = React.useState(card)

  React.useEffect(() => {
    bodyRef.current?.focus()
  }, [])

  return (
    <Modal
      onCancel={onCancel}
      header={<Header onCancel={onCancel} card={cardAffected} />}
      body={<Body ref={bodyRef} card={cardAffected} onChange={handleOnChange} />}
      footer={<Footer bodyRef={bodyRef} onCancel={onCancel} onConfirm={() => onConfirm(cardAffected)} />}
    />
  )
}
