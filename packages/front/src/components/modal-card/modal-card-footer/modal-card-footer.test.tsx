import React from 'react'
import { ModalCardFooter } from './modal-card-footer'
import { render, fireEvent, screen } from '@testing-library/react'

describe('ModalCardFooter component', () => {
  it('should call onCancel when Close button is clicked', () => {
    const bodyRef = { current: null }
    const onConfirm = jest.fn()
    const onCancel = jest.fn()

    render(<ModalCardFooter bodyRef={bodyRef} onConfirm={onConfirm} onCancel={onCancel} />)

    const closeButton = screen.getByText('Close')
    fireEvent.click(closeButton)

    expect(onCancel).toHaveBeenCalled()
  })

  it('should call onConfirm when Save Changes button is clicked', () => {
    const bodyRef = { current: null }
    const onConfirm = jest.fn()
    const onCancel = jest.fn()

    render(<ModalCardFooter bodyRef={bodyRef} onConfirm={onConfirm} onCancel={onCancel} />)

    const saveButton = screen.getByText('Save Changes')
    fireEvent.click(saveButton)

    expect(onConfirm).toHaveBeenCalled()
  })

  it('should refocus the Save Changes button when it loses focus', () => {
    const bodyRef: any = { current: { focus: jest.fn() } }
    const onConfirm = jest.fn()
    const onCancel = jest.fn()

    render(<ModalCardFooter bodyRef={bodyRef} onConfirm={onConfirm} onCancel={onCancel} />)

    const saveButton = screen.getByText('Save Changes')
    fireEvent.blur(saveButton)

    expect(bodyRef.current.focus).toHaveBeenCalled()
  })
})
