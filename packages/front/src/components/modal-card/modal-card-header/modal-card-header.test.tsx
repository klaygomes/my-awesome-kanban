import { render, fireEvent, screen } from '@testing-library/react'
import React from 'react'

import { ModalCardHeader } from './modal-card-header'

describe('ModalCardHeader', () => {
  it('renders the header with "Edit Card" title when card id exists', () => {
    const card = { id: 1, title: 'Test Card', body: 'Test body' }
    const onCancel = jest.fn()
    render(<ModalCardHeader card={card} onCancel={onCancel} />)

    expect(screen.getByText('Edit Card')).toBeInTheDocument()
  })

  it('renders the header with "New Card" title when card id does not exist', () => {
    const card = { id: undefined, title: 'Test Card', body: 'Test body' }
    const onCancel = jest.fn()
    render(<ModalCardHeader card={card} onCancel={onCancel} />)

    expect(screen.getByText('New Card')).toBeInTheDocument()
  })

  it('calls onCancel function when the close icon is clicked', () => {
    const card = { id: 1, title: 'Test Card', body: 'Test body' }
    const onCancel = jest.fn()
    render(<ModalCardHeader card={card} onCancel={onCancel} />)

    fireEvent.click(screen.getByLabelText('Cancel'))

    expect(onCancel).toHaveBeenCalled()
  })
})
