import { RiCloseCircleFill } from 'react-icons/ri'

type ModalCardHeaderProps = { card: Card; onCancel: () => void }
export const ModalCardHeader = ({ card, onCancel }: ModalCardHeaderProps) => (
  <>
    <h3 className="mb-2 text-2xl font-bold tracking-tight text-white">{card.id ? 'Edit Card' : 'New Card'}</h3>
    <button
      className="p-1 ml-auto bg-transparent border-0 float-right text-3xl leading-none font-semibold"
      onClick={onCancel}
      aria-label="Cancel"
    >
      <span className="text-gray-500">
        <RiCloseCircleFill />
      </span>
    </button>
  </>
)
