import React from 'react'
import { ModalCardBody } from './modal-card-body'
import { render, fireEvent, screen } from '@testing-library/react'

describe('ModalCardBody component', () => {
  it('should update the card title when input changes', () => {
    const card = { title: '', body: '' }
    const onChange = jest.fn()

    render(<ModalCardBody card={card} onChange={onChange} />)

    const input = screen.getByPlaceholderText('Card Title')
    fireEvent.change(input, { target: { value: 'New Card Title' } })

    expect(onChange).toHaveBeenCalledWith({ ...card, title: 'New Card Title' })
  })

  it('should update the card body when textarea changes', () => {
    const card = { title: '', body: '' }
    const onChange = jest.fn()

    render(<ModalCardBody card={card} onChange={onChange} />)

    const textarea = screen.getByPlaceholderText('Write your thoughts here...')
    fireEvent.change(textarea, { target: { value: 'New Card Body' } })

    expect(onChange).toHaveBeenCalledWith({ ...card, body: 'New Card Body' })
  })
})
