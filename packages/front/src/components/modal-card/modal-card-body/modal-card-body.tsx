import React from 'react'

type ModalCardBodyProps = { card: Card; onChange: (card: Card) => void }

export const ModalCardBody = React.forwardRef<HTMLInputElement, ModalCardBodyProps>(({ card, onChange }, ref) => {
  function handleChange(ev: React.ChangeEvent<HTMLInputElement | HTMLTextAreaElement>) {
    onChange({ ...card, [ev.target.name]: ev.currentTarget.value })
  }

  return (
    <>
      <p className="my-4 font-normal text-gray-400">
        <input
          ref={ref}
          type="text"
          name="title"
          placeholder="Card Title"
          className="px-3 py-4 relative rounded text-base border-0 shadow focus:ring w-full bg-gray-700 border-gray-600 placeholder-gray-400 text-white"
          onChange={handleChange}
        />
        <textarea
          id="message"
          name="body"
          rows={4}
          className="block p-2.5 w-full text-sm rounded-lg border  bg-gray-700 border-gray-600 placeholder-gray-400 text-white my-5"
          placeholder="Write your thoughts here..."
          onChange={handleChange}
          value={card.body}
        ></textarea>
      </p>
    </>
  )
})
