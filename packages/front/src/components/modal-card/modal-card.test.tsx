import React from 'react'
import { render, fireEvent, screen } from '@testing-library/react'
import { ModalCard } from './modal-card'

describe('ModalCard', () => {
  it('renders the header, body, and footer', () => {
    const card = { id: 1, name: 'Test Card' }
    const onConfirm = jest.fn()
    const onCancel = jest.fn()
    render(<ModalCard card={card} onConfirm={onConfirm} onCancel={onCancel} />)

    expect(screen.getByText('Edit Card')).toBeInTheDocument()
    expect(screen.getByTestId('modal-body')).toBeInTheDocument()
    expect(screen.getByText('Close')).toBeInTheDocument()
    expect(screen.getByText('Save Changes')).toBeInTheDocument()
  })

  it('calls onCancel when close button is clicked', () => {
    const card = { id: 1, name: 'Test Card' }
    const onConfirm = jest.fn()
    const onCancel = jest.fn()

    render(<ModalCard card={card} onConfirm={onConfirm} onCancel={onCancel} />)

    fireEvent.click(screen.getByText('Close'))
    expect(onCancel).toHaveBeenCalled()
  })

  it('calls onConfirm with the updated card when save changes button is clicked', () => {
    const card = { id: 1, name: 'Test Card' }
    const onConfirm = jest.fn()
    const onCancel = jest.fn()
    render(<ModalCard card={card} onConfirm={onConfirm} onCancel={onCancel} />)

    fireEvent.change(screen.getByPlaceholderText('Card Title'), {
      target: { name: 'name', value: 'Updated Test Card' }
    })
    fireEvent.click(screen.getByText('Save Changes'))
    expect(onConfirm).toHaveBeenCalledWith({ id: 1, name: 'Updated Test Card' })
  })
})
