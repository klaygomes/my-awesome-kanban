import * as React from 'react'
import { render, screen } from '@testing-library/react'
import { Board } from './board'
import { Provider, emptyContext } from '../../context'

describe('Board component', () => {
  it('renders columns and cards', () => {
    const columns = {
      ToDo: { title: 'To Do' },
      Doing: { title: 'In Progress' },
      Done: { title: 'Done' }
    }
    const cards = [
      { id: 1, kind: 'ToDo', title: 'Card 1', order: 0 },
      { id: 2, kind: 'Doing', title: 'Card 2', order: 2 },
      { id: 3, kind: 'Done', title: 'Card 3', order: 3 }
    ]
    const customContext = {
      ...emptyContext,
      onMove: jest.fn(),
      cards
    }
    const requestAddCard = jest.fn()
    render(
      <Provider value={customContext}>
        <Board columns={columns} requestAddCard={requestAddCard} />
      </Provider>
    )
    expect(screen.getByText('To Do')).toBeInTheDocument()
    expect(screen.getByText('In Progress')).toBeInTheDocument()
    expect(screen.getByText('Done')).toBeInTheDocument()
    expect(screen.getByText('Card 1')).toBeInTheDocument()
    expect(screen.getByText('Card 2')).toBeInTheDocument()
    expect(screen.getByText('Card 3')).toBeInTheDocument()
  })
})
