import * as R from 'ramda'
import React from 'react'

import { DragDropContext, Droppable, Draggable } from '@hello-pangea/dnd'

import { Column } from '../column'
import { Card } from '../card'
import { Context } from '../../context'

type BoardEvents = {
  requestAddCard: (kind: ColumnKind) => void
}

type BoardProps = {
  columns: { [P in ColumnKind]: Column }
} & BoardEvents

export const Board = ({ columns, requestAddCard }: BoardProps) => {
  const { onMove, cards } = React.useContext(Context)
  return (
    <DragDropContext onDragEnd={onMove}>
      {Object.entries(columns).map(([kind, column]) => (
        <Column
          cards={R.filter(R.propEq('kind', kind))(cards)}
          column={column}
          key={kind}
          requestAddCard={requestAddCard}
        >
          {columnCards => (
            <Droppable droppableId={kind}>
              {provided => (
                <div {...provided.droppableProps} ref={provided.innerRef} className="px-4 pt-4">
                  {columnCards.map(card => (
                    <Draggable
                      key={card.kind.concat(String(card.id), String(card.order))}
                      draggableId={String(card.id)}
                      index={card.order}
                    >
                      {provided => (
                        <Card
                          ref={provided.innerRef}
                          key={String(card.id).concat(String(card.order))}
                          style={provided.draggableProps.style}
                          {...Object.assign({}, card, provided.dragHandleProps, provided.draggableProps)}
                        />
                      )}
                    </Draggable>
                  ))}
                  {provided.placeholder}
                </div>
              )}
            </Droppable>
          )}
        </Column>
      ))}
    </DragDropContext>
  )
}
