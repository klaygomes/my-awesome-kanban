/* eslint-disable @typescript-eslint/no-non-null-assertion */
/* eslint-disable testing-library/no-node-access */

import React from 'react'
import { render, act, cleanup, fireEvent, screen } from '@testing-library/react'

import { CustomError } from './custom-error'

jest.useFakeTimers()

describe('CustomError', () => {
  afterEach(cleanup)

  it('displays an error message and disappears after 5 seconds', () => {
    render(
      <>
        <CustomError />
        <div data-testid="sibling"></div>
      </>
    )

    // Dispatch the customError event
    fireEvent(document, new CustomEvent('customError', { detail: 'Something went wrong' }))

    expect(screen.getByText('Something went wrong.')).toBeInTheDocument()

    // advance time by 5.001 seconds
    act(() => {
      jest.advanceTimersByTime(5001)
    })
    expect(screen.queryByText('Something went wrong.')).toBeNull()
  })

  it('should hide error message on click', () => {
    const errorMessage = 'This is a custom error'
    render(<CustomError />)

    // Dispatch the customError event
    fireEvent(document, new CustomEvent('customError', { detail: errorMessage }))

    // Verify that the error message is displayed
    const errorElement = screen.getByText('This is a custom error.')
    expect(errorElement).toBeVisible()

    // Click on the error message
    fireEvent.click(screen.getByRole('alert'))

    // Verify that the error message is no longer displayed
    expect(errorElement).not.toBeInTheDocument()
  })
})
