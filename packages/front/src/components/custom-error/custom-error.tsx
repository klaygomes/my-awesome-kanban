import { RiInformationFill } from 'react-icons/ri'
import React from 'react'

export const CustomError = () => {
  const [customError, setCustomError] = React.useState(null)

  React.useEffect(() => {
    let idx: NodeJS.Timeout
    const fireEvent = (e: any) => {
      const { detail } = e
      setCustomError(detail)
      idx = setTimeout(() => setCustomError(null), 5000)
    }
    document.addEventListener('customError', fireEvent)
    return () => {
      clearTimeout(idx)
      document.removeEventListener('customError', fireEvent)
    }
  }, [])

  if (!customError) return null

  return (
    <div
      className="fixed inset-0 z-50 cursor-pointer h-14 flex p-4 mb-4 text-sm bg-red-900 text-red-100"
      role="alert"
      onClick={() => setCustomError(null)}
    >
      <RiInformationFill width={40} height={40} className="inline-block mx-2 mt-1" />
      <span className="sr-only"> Info </span>
      <div>
        <span className="font-medium"> Opps... </span> {customError}.
      </div>
    </div>
  )
}
