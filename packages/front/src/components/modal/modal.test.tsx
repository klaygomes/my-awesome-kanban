import React from 'react'
import { render, fireEvent, screen } from '@testing-library/react'
import { Modal } from './modal'

describe('Modal component', () => {
  const header = <div data-testid="header">header</div>
  const body = <input data-testid="body" placeholder="body" />
  const footer = <div data-testid="footer">footer</div>

  const onCancel = jest.fn()

  it('renders header, body, and footer', () => {
    render(<Modal header={header} body={body} footer={footer} onCancel={jest.fn()} />)

    expect(screen.getByTestId('header')).toHaveTextContent('header')
    expect(screen.getByPlaceholderText('body')).toBeInTheDocument()
    expect(screen.getByTestId('footer')).toHaveTextContent('footer')
  })

  it('should call onCancel when Esc key is pressed', () => {
    render(<Modal header={header} body={body} footer={footer} onCancel={onCancel} />)

    fireEvent.focus(screen.getByTestId('body'))
    fireEvent.keyDown(screen.getByTestId('body'), { key: 'Escape', code: 'Escape' })

    expect(onCancel).toHaveBeenCalled()
  })
})
