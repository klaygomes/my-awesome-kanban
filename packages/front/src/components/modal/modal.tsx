import React from 'react'

type ModalProps = {
  header: React.ReactElement
  body: React.ReactElement
  footer: React.ReactElement
  onCancel: () => void
}

export const Modal: React.FC<ModalProps> = ({ header, body, footer, onCancel }) => {
  function handleEsc(ev: React.KeyboardEvent) {
    if (ev.key === 'Escape') {
      onCancel()
    }
  }

  return (
    <>
      <div
        onKeyDown={handleEsc}
        className="justify-center items-center flex overflow-x-hidden overflow-y-auto fixed inset-0 z-50"
      >
        <div className="relative my-6 mx-auto w-1/4">
          <div className="border-0 rounded-lg shadow-lg relative flex flex-col w-full bg-gray-800">
            <div
              data-testid="modal-header"
              className="flex items-start justify-between p-5 border-b border-solid border-gray-700 rounded-t"
            >
              {header}
            </div>
            <div data-testid="modal-body" className="relative p-6 flex-auto">
              {body}
            </div>
            <div
              data-testid="modal-footer"
              className="flex items-center justify-end p-6 border-t border-solid border-gray-700 rounded-b"
            >
              {footer}
            </div>
          </div>
        </div>
      </div>
      <div className="opacity-75 fixed inset-0 z-40 bg-gray-900"></div>
    </>
  )
}
