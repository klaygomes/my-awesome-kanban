import React from 'react'
import * as R from 'ramda'
import { RiAddCircleLine } from 'react-icons/ri'

export type ColumnsEvents = {
  requestAddCard: (kind: ColumnKind) => void
}
export type ColumnProps = {
  cards: Card[]
  column: Column
  children: (cards: Card[]) => React.ReactNode
} & ColumnsEvents

export const Column: React.FC<ColumnProps> = ({ column, cards, children, requestAddCard }) => {
  return (
    <div className="mx-auto w-full bg-gray-900 rounded-t-2xl border border-gray-700 shadow">
      <div className="flex justify-around	p-4 bg-gray-800 rounded-t-2xl">
        <div className="flex-grow-0 w-fit" role="heading" aria-level={2}>
          <span className="text-white text-2xl font-bold tracking-tight whitespace-nowrap mx-2">{column.title}</span>
          <span className="text-xs font-medium  px-2.5 py-0.5 rounded-full bg-gray-700 text-gray-400 border border-gray-500 align-text-top">
            {cards.length}
          </span>
        </div>
      </div>
      <div
        className="mt-4 mx-4 border rounded-lg border-gray-700 p-2 cursor-pointer text-center"
        onClick={() => requestAddCard(column.kind)}
      >
        <RiAddCircleLine className="inline-block text-white mx-2" />
        <span className="inline font-normal text-gray-400">Add card</span>
      </div>
      {children(R.sortBy(R.prop('order'), cards))}
    </div>
  )
}
