import React from 'react'
import { render, fireEvent, screen } from '@testing-library/react'
import * as R from 'ramda'
import { Column } from './column'

describe('Column component', () => {
  const cards = [
    { id: '1', title: 'Card 1', order: 1, kind: 'column-1' },
    { id: '2', title: 'Card 2', order: 2, kind: 'column-1' },
    { id: '3', title: 'Card 3', order: 3, kind: 'column-1' }
  ]
  const column = { title: 'Column 1', kind: 'column-1' }
  const requestAddCard = jest.fn()

  const renderComponent = (props = {}) => {
    const allProps = { cards, column, requestAddCard, children: props => <div>{JSON.stringify(props)}</div>, ...props }
    return render(<Column {...allProps} />)
  }

  it('should display the title and number of cards', () => {
    renderComponent()
    expect(screen.getByText(column.title)).toBeInTheDocument()
    expect(screen.getByText(`${cards.length}`)).toBeInTheDocument()
  })

  it('should call the requestAddCard function when the add card button is clicked', () => {
    renderComponent()
    fireEvent.click(screen.getByText('Add card'))
    expect(requestAddCard).toHaveBeenCalledWith(column.kind)
  })

  it('should render the children with sorted cards', () => {
    const children = jest.fn(props => <div>{JSON.stringify(props)}</div>)
    renderComponent({ children })
    expect(children).toHaveBeenCalledWith(R.sortBy(R.prop('order'), cards))
  })
})
