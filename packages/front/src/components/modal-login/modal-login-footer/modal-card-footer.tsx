type ModalCartFooterPros = {
  bodyRef: React.RefObject<HTMLInputElement | null>
  onConfirm: () => void
}
export const ModalLoginFooter = ({ bodyRef, onConfirm }: ModalCartFooterPros) => {
  function handleBlur() {
    bodyRef.current?.focus()
  }

  return (
    <>
      <button
        className="bg-gray-500 text-white active:bg-gray-800 font-bold uppercase text-sm focus:outline px-6 py-3 rounded shadow hover:shadow-lg mr-1 mb-1 ease-linear transition-all duration-150"
        type="button"
        onClick={onConfirm}
        onBlur={handleBlur}
      >
        Login
      </button>
    </>
  )
}
