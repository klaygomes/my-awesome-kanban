import React from 'react'
import { render, fireEvent, screen } from '@testing-library/react'
import { ModalLoginFooter } from './modal-card-footer'

describe('ModalLoginFooter', () => {
  const onConfirm = jest.fn()
  const bodyRef: any = { current: {} }

  it('should call onConfirm when clicking the Login button', () => {
    render(<ModalLoginFooter bodyRef={bodyRef} onConfirm={onConfirm} />)
    const button = screen.getByText('Login')

    fireEvent.click(button)

    expect(onConfirm).toHaveBeenCalled()
  })

  it('should refocus the body when blurring the Login button', () => {
    bodyRef.current = { focus: jest.fn() }

    render(<ModalLoginFooter bodyRef={bodyRef} onConfirm={onConfirm} />)
    const button = screen.getByText('Login')

    fireEvent.blur(button)

    expect(bodyRef.current?.focus).toHaveBeenCalled()
  })
})
