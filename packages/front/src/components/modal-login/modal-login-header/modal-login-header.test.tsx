import React from 'react'
import { render, fireEvent, screen } from '@testing-library/react'
import { ModalLoginHeader } from './modal-login-header'

describe('ModalLoginHeader', () => {
  it('calls onCancel when the close button is clicked', () => {
    const onCancel = jest.fn()
    render(<ModalLoginHeader onCancel={onCancel} />)

    fireEvent.click(screen.getByLabelText('Cancel'))

    expect(onCancel).toHaveBeenCalled()
  })
})
