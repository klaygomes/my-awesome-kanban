import React from 'react'

import { Modal } from '../modal'
import { ModalLoginHeader as Header } from './modal-login-header'
import { ModalLoginBody as Body } from './modal-login-body'
import { ModalLoginFooter as Footer } from './modal-login-footer'

type ModalLoginProps = { user: User; onConfirm: (user: User) => void }
export const ModalLogin = ({ user, onConfirm }: ModalLoginProps) => {
  const bodyRef = React.useRef<HTMLInputElement | null>(null)
  const [userAffected, handleOnChange] = React.useState(user)

  React.useEffect(() => {
    bodyRef.current?.focus()
  }, [])

  function noop() {
    /**/
  }

  return (
    <Modal
      onCancel={noop}
      header={<Header onCancel={noop} />}
      body={<Body ref={bodyRef} user={userAffected} onChange={handleOnChange} />}
      footer={<Footer bodyRef={bodyRef} onConfirm={() => onConfirm(userAffected)} />}
    />
  )
}
