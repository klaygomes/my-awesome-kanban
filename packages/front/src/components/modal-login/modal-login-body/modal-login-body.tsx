import React from 'react'

type ModalLoginBodyProps = { user: User; onChange: (user: User) => void }

export const ModalLoginBody = React.forwardRef<HTMLInputElement, ModalLoginBodyProps>(({ user, onChange }, ref) => {
  function handleChange(ev: React.ChangeEvent<HTMLInputElement | HTMLTextAreaElement>) {
    onChange({ ...user, [ev.target.name]: ev.currentTarget.value })
  }

  return (
    <>
      <p className="font-normal text-gray-400">You must login before continue</p>
      <p className="my-4 font-normal text-gray-400">
        <input
          ref={ref}
          type="text"
          name="user"
          placeholder="Login"
          className="px-3 py-4 mb-4 relative rounded text-base border-0 shadow focus:ring w-full bg-gray-700 border-gray-600 placeholder-gray-400 text-white"
          onChange={handleChange}
        />
        <input
          ref={ref}
          type="password"
          name="password"
          placeholder="Password"
          className="px-3 py-4 relative rounded text-base border-0 shadow focus:ring w-full bg-gray-700 border-gray-600 placeholder-gray-400 text-white"
          onChange={handleChange}
        />
      </p>
    </>
  )
})
