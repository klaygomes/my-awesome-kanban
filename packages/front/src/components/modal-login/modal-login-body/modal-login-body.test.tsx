import React from 'react'
import { render, fireEvent, screen } from '@testing-library/react'
import { ModalLoginBody } from './modal-login-body'

describe('ModalLoginBody component', () => {
  const onChange = jest.fn()
  const user = { user: '', password: '' }

  it('renders two inputs', () => {
    render(<ModalLoginBody user={user} onChange={onChange} />)

    expect(screen.getByPlaceholderText('Login')).toBeInTheDocument()
    expect(screen.getByPlaceholderText('Password')).toBeInTheDocument()
  })

  it('calls onChange when an input value changes', () => {
    render(<ModalLoginBody user={user} onChange={onChange} />)

    fireEvent.change(screen.getByPlaceholderText('Login'), {
      target: { name: 'user', value: 'John Doe' }
    })

    expect(onChange).toHaveBeenCalledWith({ ...user, user: 'John Doe' })
  })

  it('calls onChange when an password value changes', () => {
    render(<ModalLoginBody user={user} onChange={onChange} />)

    fireEvent.change(screen.getByPlaceholderText('Password'), {
      target: { name: 'password', value: '$3crete' }
    })

    expect(onChange).toHaveBeenCalledWith({
      ...user,
      password: '$3crete'
    })
  })
})
