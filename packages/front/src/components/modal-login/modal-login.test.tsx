import React from 'react'
import { render, fireEvent, screen, within } from '@testing-library/react'

import { ModalLogin as Login } from './modal-login'

test('ModalLogin should render properly', () => {
  const user = { user: '', password: '' }
  const onConfirm = jest.fn()

  render(<Login user={user} onConfirm={onConfirm} />)

  const userInput = screen.getByPlaceholderText<HTMLInputElement>('Login')
  const passwordInput = screen.getByPlaceholderText<HTMLInputElement>('Password')
  const submitButton = within(screen.getByTestId('modal-footer')).getByText('Login')

  fireEvent.change(userInput, { target: { value: 'john' } })
  expect(userInput.value).toBe('john')
  fireEvent.change(passwordInput, { target: { value: 'secret' } })

  expect(passwordInput.value).toBe('secret')
  fireEvent.click(submitButton)

  expect(onConfirm).toHaveBeenCalledWith({ user: 'john', password: 'secret' })
})
