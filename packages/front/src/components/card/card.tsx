import React from 'react'
import { RiEditLine, RiSaveLine, RiDeleteBinLine } from 'react-icons/ri'
import { clsx } from 'clsx'
import { useAutosizeTextArea } from '../../utils/use-autosize-text-area'
import { Context } from '../../context'
import { showError } from '../../utils/show-error'

type CardProps = {
  [str: string]: unknown
  style?: React.CSSProperties
} & Card

const editBorder = 'border-gray-600 cursor-text'

export const Card = React.forwardRef<HTMLDivElement, CardProps>((rest, ref) => {
  const { onEdit, onDiscard } = React.useContext(Context)

  const { id, title, body, order, kind, style, ...props } = rest
  const [readonly, setCardState] = React.useState(true)
  const [cardTitle, setTitle] = React.useState(title)
  const [cardBody, setBody] = React.useState(body)
  const textAreaRef = React.useRef<HTMLTextAreaElement>(null)

  const handleFocus = (ev: React.FocusEvent<HTMLInputElement | HTMLTextAreaElement>) =>
    readonly && ev.currentTarget.blur()
  const setEditable = () => setCardState(false)
  const setReadOnly = () => setCardState(true)

  // we do an optimistic edit and remove approach
  const handleEdit = (card: Card) => {
    if (!String(card.title).trim() || !String(card.body).trim()) {
      showError('Card title and body are required')
      return
    }
    setReadOnly()
    onEdit({
      ...card,
      title: cardTitle,
      body: cardBody
    })
  }

  const handleDiscard = (card: Card) => {
    if (!window.confirm('Confirm?')) return
    setReadOnly()
    onDiscard(card)
  }

  useAutosizeTextArea(textAreaRef.current, cardBody, readonly)

  return (
    <div
      {...props}
      ref={ref}
      style={{ ...style, userSelect: 'none' }}
      className="p-2 mb-4 border rounded-lg border-gray-700 shadow bg-gray-800  cursor-grab"
      onDoubleClick={setEditable}
    >
      <div className="mb-2 text-2xl font-bold tracking-tight text-white flex flex-space-between">
        <h2 className="flex-grow">
          {readonly ? (
            <span className="inline-block p-2 bg-gray-800 text-white outline-none border border-gray-800 tracking-normal">
              {cardTitle}
            </span>
          ) : (
            <input
              type="text"
              className={clsx(
                'w-full p-2 pl-2 bg-gray-800 text-white outline-none border border-gray-700 rounded',
                !readonly && editBorder
              )}
              value={cardTitle}
              onFocus={handleFocus}
              onChange={ev => setTitle(ev.currentTarget.value)}
            />
          )}
        </h2>
        {readonly ? (
          <RiEditLine className="align-bottom" onClick={setEditable} role="button" title="Edit this card" />
        ) : (
          <RiSaveLine
            className="align-text-bottom ml-2"
            onClick={() => handleEdit({ id, title, body, order, kind })}
            role="button"
            title="Save this card"
          />
        )}
      </div>
      {readonly ? (
        <span className="p-2 mb-3 font-normal outline-0 bg-gray-800 text-gray-400 w-full resize-none border border-gray-800">
          {cardBody}
        </span>
      ) : (
        <textarea
          ref={textAreaRef}
          className={clsx(
            'p-2 mb-3 font-normal outline-0 bg-gray-800 text-gray-400 w-full resize-none border border-gray-700 rounded scroll-m-0',
            !readonly && editBorder
          )}
          value={cardBody}
          onFocus={handleFocus}
          onChange={ev => setBody(ev.currentTarget.value)}
        />
      )}
      <RiDeleteBinLine
        className="align-bottom text-white mt-2"
        onClick={() => handleDiscard({ id, title, body, order, kind })}
        role="button"
        title="Delete this card"
      />
    </div>
  )
})
