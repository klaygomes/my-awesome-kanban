import React from 'react'
import { render, screen, within } from '@testing-library/react'
import userEvent from '@testing-library/user-event'

import { Card } from '.'
import { Provider, emptyContext } from '../../context'

describe('Card', () => {
  it('should mount without errors', async () => {
    const card = {
      id: '1',
      title: 'Some title',
      body: 'Some body',
      kind: 'ToDo'
    }

    // some properties
    const props = {
      tabIndex: 1000,
      role: 'banner'
    }

    // some style
    const style = {
      background: 'red'
    }

    // actual props
    const actualProps = Object.assign({}, card, props, { style })

    render(
      <Provider value={emptyContext}>
        <Card {...actualProps} />
      </Provider>
    )

    const mainContainer = await screen.findByRole('banner')
    const title = await screen.findByText(card.title)
    const body = await screen.findByText(card.body)

    // the properties are being spread into main element?
    expect(mainContainer).toBeInTheDocument()
    expect(mainContainer).toHaveStyle(style)
    expect(mainContainer.tabIndex).toBe(props.tabIndex)

    // we have textual card data?
    expect(title).toBeInTheDocument()
    expect(body).toBeInTheDocument()
  })

  it('click event is being fired', async () => {
    const card = {
      id: '1',
      title: 'Some title',
      body: 'Some body',
      kind: 'ToDo'
    }

    const customContext = {
      ...emptyContext,
      onEdit: jest.fn(),
      cards: [card]
    }
    render(
      <Provider value={customContext}>
        <Card {...card} />
      </Provider>
    )
    await userEvent.click(screen.getByTitle('Edit this card'))
    await userEvent.click(screen.getByTitle('Save this card'))

    expect(customContext.onEdit).toHaveBeenCalledWith(card)
  })
})
