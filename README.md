Welcome to our Kanban app! Get organized and increase your productivity by managing your tasks with ease.

<center>
![My Awesome Kanban](/docs/kanban.gif)
</center>

## Before you start

You will have to set the credentials for the backend project. It can be done by editing the file '.env', e.g:

```
JWT_SECRET=4ee01ac6a4e44dfaa182e92abbad7f79
DEFAULT_LOGIN=letscode
DEFAULT_PASSWORD=lets@123
```

Install all required deps by running `yarn install`.

### Requirements

- Mac, Linux or Windows;
- Make sure you have the latest changes pulled from master;
- an updated version of yarn cli (=>1.22.0 ) 
- Node (>=18)

## How to start

If you have all requirements, to start the application it is a matter of:

```bash
yarn && yarn start
```

you can also run each `workspace` individually by running `yarn start` in its folder or `yarn workspace @ada-tech/[project-name] start` everywhere else, eg. to start frontend runs:

```bash
yarn workspace @ada-tech/front start
```

## Lint

You can lint files by running `yarn lint` at the project root, `yarn lint` at the workspace path or `yarn workspace @ada-tech/[project-name] lint`.

If you want to fix issues you can append `:fix` at the end of this command. e.g:

`yarn lint:fix` or `yarn workspace @ada-tech/front lint:fix`


# Chosen Technologies

- Tailwind CSS
- React
- Yarn workspaces

## Tailwind CSS

Tailwind CSS is a powerful and efficient low-level CSS framework that offers a great solution for building web applications. One of its standout features is its performance, which has been optimized to ensure that it won't slow down this example application. Tailwind CSS offers a wealth of utility classes that make it easy to add styles to your components, reducing the amount of time I would have to spend writing CSS from scratch.

## React

React makes it painless to create interactive UIs. Design simple views for each state in your application, and React will efficiently update and render just the right components when your data changes.

## Eslint + Prettier

An opinionated code formatter, supports many languages, integrates with most editors, has few options.

## Yarn workspaces

Yarn workspaces are a valuable tool for managing multiple packages in a single repository. They improve collaboration by allowing multiple developers to work on separate packages within the same repository, reducing the need for managing multiple repositories. Additionally, Yarn workspaces improve build time by allowing packages to be built and linked together within the repository, reducing repetitive and time-consuming builds. 

### Directory representation

```text
+--my-awesome-kanban
|   +-- packages
|       +-- back
|          --- package.json
|          --- tsconfig.json
|       +-- front
|          --- package.json
|          --- tsconfig.json
|       +-- core
|          --- package.json
|          --- tsconfig.json
|       +--[package-name]
|          --- package.json
|          --- tsconfig.json
|   --- package.json
|   --- tsconfig.json
|   --- *
```

### back

Backend implementation

### front

Create react app application

### Core

Core utility and common features between projects

# What is missing

Because of lack of time (I had actually only two days to work on this solution), it was not possible to:

- front/hoc/with-http-service has room for improvement. A better approach could be to inject HttpService instead of hard coding it on its prolog.
- Write more tests, there are a lot of places where I would improve the test surface, but it would take much more time than I had, for example, to gain some time some of the tests where written using pure js (so I do not lose time fighting against type system 🤠);
- Improve accessibility, although some components are accessible, modal for example, most of the others components I did not have time to work on its accessibility'
- Unfortunately I did not have enough time to implement i18n.


## Test coverage

<pre>
-----------------------------------------------|---------|----------|---------|---------|-------------------------
File                                           | % Stmts | % Branch | % Funcs | % Lines | Uncovered Line #s
-----------------------------------------------|---------|----------|---------|---------|-------------------------
All files                                      |   50.21 |    42.62 |   51.54 |   50.94 |
 src                                           |    11.9 |        0 |       0 |   13.51 |
  Kanban.tsx                                   |       0 |        0 |       0 |       0 | 12-66
  context.ts                                   |      50 |      100 |       0 |      50 | 18-30
  index.tsx                                    |       0 |      100 |     100 |       0 | 9-13
 src/components/board                          |     100 |      100 |     100 |     100 |
  board.tsx                                    |     100 |      100 |     100 |     100 |
  index.ts                                     |       0 |        0 |       0 |       0 |
 src/components/card                           |   67.74 |    72.22 |      50 |   67.85 |
  card.tsx                                     |   67.74 |    72.22 |      50 |   67.85 | ...-33,44-46,74,102-107
  index.tsx                                    |       0 |        0 |       0 |       0 |
 src/components/column                         |     100 |      100 |     100 |     100 |
  column.tsx                                   |     100 |      100 |     100 |     100 |
  index.ts                                     |       0 |        0 |       0 |       0 |
 src/components/custom-error                   |     100 |      100 |     100 |     100 |
  custom-error.tsx                             |     100 |      100 |     100 |     100 |
  index.ts                                     |       0 |        0 |       0 |       0 |
 src/components/modal                          |     100 |       50 |     100 |     100 |
  index.ts                                     |       0 |        0 |       0 |       0 |
  modal.tsx                                    |     100 |       50 |     100 |     100 | 12
 src/components/modal-card                     |     100 |      100 |     100 |     100 |
  index.ts                                     |       0 |        0 |       0 |       0 |
  modal-card.tsx                               |     100 |      100 |     100 |     100 |
 src/components/modal-card/modal-card-body     |     100 |      100 |     100 |     100 |
  index.ts                                     |       0 |        0 |       0 |       0 |
  modal-card-body.tsx                          |     100 |      100 |     100 |     100 |
 src/components/modal-card/modal-card-footer   |     100 |      100 |     100 |     100 |
  index.ts                                     |       0 |        0 |       0 |       0 |
  modal-card-footer.tsx                        |     100 |      100 |     100 |     100 |
 src/components/modal-card/modal-card-header   |     100 |      100 |     100 |     100 |
  index.ts                                     |       0 |        0 |       0 |       0 |
  modal-card-header.tsx                        |     100 |      100 |     100 |     100 |
 src/components/modal-login                    |     100 |      100 |      75 |     100 |
  index.ts                                     |       0 |        0 |       0 |       0 |
  modal-login.tsx                              |     100 |      100 |      75 |     100 |
 src/components/modal-login/modal-login-body   |     100 |      100 |     100 |     100 |
  index.ts                                     |       0 |        0 |       0 |       0 |
  modal-login-body.tsx                         |     100 |      100 |     100 |     100 |
 src/components/modal-login/modal-login-footer |     100 |      100 |     100 |     100 |
  index.ts                                     |       0 |        0 |       0 |       0 |
  modal-card-footer.tsx                        |     100 |      100 |     100 |     100 |
 src/components/modal-login/modal-login-header |     100 |      100 |     100 |     100 |
  index.ts                                     |       0 |        0 |       0 |       0 |
  modal-login-header.tsx                       |     100 |      100 |     100 |     100 |
 src/hoc                                       |       0 |        0 |       0 |       0 |
  with-http-service.tsx                        |       0 |        0 |       0 |       0 | 12-122
 src/service                                   |   68.57 |       50 |   58.82 |   70.37 |
  empty-service.ts                             |       0 |      100 |       0 |       0 | 1-12
  http-service.ts                              |      80 |       50 |   76.92 |   86.36 | 48-54
 src/utils                                     |     100 |      100 |     100 |     100 |
  show-error.ts                                |     100 |      100 |     100 |     100 |
  use-autosize-text-area.ts                    |     100 |      100 |     100 |     100 |
-----------------------------------------------|---------|----------|---------|---------|-------------------------

Test Suites: 16 passed, 16 total
Tests:       37 passed, 37 total
Snapshots:   0 total
Time:        2.172 s
Ran all test suites related to changed files.

</pre>



