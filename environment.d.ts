declare module 'ramda'

type ColumnKind = 'ToDo' | 'Doing' | 'Done'

type Column = {
  title: string
  kind: ColumnKind
}

type Card = {
  id?: string
  order: number
  title: string
  body: string
  kind: ColumnKind
}

type User = {
  user?: string;
  password?: string;
  token?: string;
}

interface Crud<T> {
  create: (record: Partial<T>) => Promise<T[]>
  read: () => Promise<T[]>
  update: (record: Partial<T>) => Promise<T>
  // delete is a javascript reserved word, using discard instead
  discard: (record: Partial<T>) => Promise<T[]>
}

interface Authentication<T> {
  login: (user: T) => Promise<T>
  logOut: (user: T) => void
}

/* NodeJS related argumentation */
declare namespace NodeJS {
  export interface ProcessEnv {
    DEFAULT_LOGIN: string
    DEFAULT_PASSWORD: string
    JWT_SECRET: string
  }
}

interface CustomEventMap {
  "customerrror": CustomEvent<{detail:string}>;
}
declare global {
  interface Document { //adds definition to Document, but you can do the same with HTMLElement
    addEventListener<K extends keyof CustomEventMap>(type: K,
      listener: (this: Document, ev: CustomEventMap[K]) => void): void;
    dispatchEvent<K extends keyof CustomEventMap>(ev: CustomEventMap[K]): void;
  }
}
